import React from 'react';

class Formulario extends React.Component{
  constructor(props){
    super();
    this.state = {
      input:'',
      checkbox: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
  }
  handleInputChange(event){
    this.setState({input:event.target.value});
  }

  handleCheckBoxChange(event){
    this.setState({checkbox: event.target.checked});
  }

  render(){
    return (
    <>
      <form>
        <label htmlFor="inpu">Este es el input</label>
        <input id="inpu" type="text" value={this.state.input} onChange={this.handleInputChange}/>
        <label htmlFor="chec">Checkbox</label>
        <input id="chec" type="checkbox" checked={this.state.checkbox} onChange={this.handleCheckBoxChange}/>
      </form>
    </>
    );
  }
}

export default Formulario;
